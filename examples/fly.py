#!/usr/bin/python

import rospy
import math
from kair_drone.msg import Pose


rospy.init_node("fly")
pub = rospy.Publisher("position_cmd", Pose, queue_size=100)
rate = rospy.Rate(100)
t = 0.0
a1 = 1
a2 = 1
a3 = 0
w1 = 1
w2 = 1
w3 = 1
u = 0.25
while not rospy.is_shutdown():
  x = 0 + a1*math.sin(w1*t)
  y = 0 + a2*math.cos(w2*t)
  z = 1 + a3*math.sin(w3*t)
  t = t + u * 0.01
  
  cmd = Pose()
  cmd.x = x
  cmd.y = y
  cmd.z = z
  pub.publish(cmd)
  
  rate.sleep()
