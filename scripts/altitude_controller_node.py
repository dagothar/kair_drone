#!/usr/bin/python

import rospy
import numpy as np
from std_msgs.msg import Float32
from kair_drone.msg import Pose

from pid.pid import PID
from dynamic_reconfigure.server import Server
from kair_drone.cfg import AltitudeControllerConfig
from kair_drone.srv import Reset, ResetResponse
from kair_drone.srv import Enable, EnableResponse
import math


pose = Pose()
alt_desired = 0.0
enabled = True


def pose_cb(msg):
  global pose
  pose = msg
  

def alt_desired_cb(msg):
  global alt_desired
  alt_desired = msg.data


def reset_handle(req, alt_pid):
  alt_pid.reset()
  return ResetResponse()


def enable_handle(req, alt_pid):
  global enabled
  
  enabled = req.enable
  if enabled:
    alt_pid.reset()
  return EnableResponse()


def reconfigure(config, level, alt_pid):
  alt_pid._kp = config.alt_P
  alt_pid._ki = config.alt_I
  alt_pid._kd = config.alt_D
  
  return config
  

def main():
  rospy.init_node("altitude_controller")
  
  controllers = rospy.get_param("~altitude_controller")  
  alt_pid = PID(**controllers["alt_pid"])
  gravity_compensation = rospy.get_param("~altitude_controller/gravity_compensation", 9.8)
  
  pose_sub = rospy.Subscriber("pose", Pose, pose_cb)
  alt_desired_sub = rospy.Subscriber("alt_desired", Float32, alt_desired_cb)
  z_force_cmd_pub = rospy.Publisher("z_force_cmd", Float32, queue_size=100)
  
  reset_srv = rospy.Service("~reset", Reset, lambda req: reset_handle(req, alt_pid))
  enable_srv = rospy.Service("~enable", Enable, lambda req: enable_handle(req, alt_pid))
  
  dynamic_reconfigure_server = Server(AltitudeControllerConfig, lambda c, l: reconfigure(c, l, alt_pid))
  
  loop_rate = rospy.Rate(100)
  while not rospy.is_shutdown():
    if enabled:
      
      # update controllers
      z_force = alt_pid.update(pose.z, alt_desired, 0.01) + gravity_compensation
      
      # send command
      cmd = Float32(z_force)
      z_force_cmd_pub.publish(cmd)
    
    try:
      loop_rate.sleep()
    except rospy.ROSTimeMovedBackwardsException:
      pass
  

if __name__ == "__main__":
  main()
