#!/usr/bin/python

import rospy
import numpy as np
from tf.transformations import *
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float32
from std_msgs.msg import String
from kair_drone.msg import Pose


def set_mode(mode):
  if mode == ""
  pass
  

def main():
  rospy.init_node("mode_controller")
  
  rate = rospy.get_param("~rate", 100.0)
  
  mode_pub = rospy.Publisher("mode", String, queue_size=100)
  
  loop_rate = rospy.Rate(rate)
  while not rospy.is_shutdown():
    loop_rate.sleep()
  

if __name__ == "__main__":
  main()
