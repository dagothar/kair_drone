#!/usr/bin/python

import rospy
import numpy as np
from tf.transformations import *
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float32
from kair_drone.msg import Pose
from pid.pid import PID
from dynamic_reconfigure.server import Server
from kair_drone.cfg import AttitudeControllersConfig
from kair_drone.srv import Reset, ResetResponse
from kair_drone.srv import Enable, EnableResponse


enabled = True
pose = Pose()
attitude_cmd = Pose()
z_force_cmd = 0.0
manual_attitude_cmd = Pose()


def pose_cb(msg):
  global pose
  pose = msg
  

def attitude_cmd_cb(msg):
  global attitude_cmd
  attitude_cmd = msg


def z_force_cmd_cb(msg):
  global z_force_cmd
  z_force_cmd = msg.data


def reset_handle(req, roll_pid, pitch_pid, yaw_pid):
  roll_pid.reset()
  pitch_pid.reset()
  yaw_pid(reset)
  return ResetResponse()


def enable_handle(req, alt_pid):
  global enabled
  
  enabled = req.enable
  if enabled:
    roll_pid.reset()
    pitch_pid.reset()
    yaw_pid(reset)
  return EnableResponse()
  

def reconfigure(config, level, roll_pid, pitch_pid, yaw_pid):
  roll_pid._kp = config.roll_P
  roll_pid._ki = config.roll_I
  roll_pid._kd = config.roll_D
  
  pitch_pid._kp = config.pitch_P
  pitch_pid._ki = config.pitch_I
  pitch_pid._kd = config.pitch_D
  
  yaw_pid._kp = config.yaw_P
  yaw_pid._ki = config.yaw_I
  yaw_pid._kd = config.yaw_D
  
  return config
  

def main():
  rospy.init_node("attitude_controller")
  
  controllers = rospy.get_param("~attitude_controllers")  
  roll_pid = PID(**controllers["roll_pid"])
  pitch_pid = PID(**controllers["pitch_pid"])
  yaw_pid = PID(**controllers["yaw_pid"])
  rate = rospy.get_param("~rate", 100.0)
  
  pose_sub = rospy.Subscriber("pose", Pose, pose_cb)
  attitude_cmd_sub = rospy.Subscriber("attitude_cmd", Pose, attitude_cmd_cb)
  z_force_cmd_sub = rospy.Subscriber("z_force_cmd", Float32, z_force_cmd_cb)
  wrench_pub = rospy.Publisher("wrench_cmd", WrenchStamped, queue_size=100)
  
  reset_srv = rospy.Service("~reset", Reset, lambda req: reset_handle(req, roll_pid, pitch_pid, yaw_pid))
  enable_srv = rospy.Service("~enable", Enable, lambda req: enable_handle(req, roll_pid, pitch_pid, yaw_pid))
  
  dynamic_reconfigure_server = Server(AttitudeControllersConfig, lambda c, l: reconfigure(c, l, roll_pid, pitch_pid, yaw_pid))
  
  loop_rate = rospy.Rate(rate)
  dt = 1.0 / rate;
  while not rospy.is_shutdown():
    roll_cmd = attitude_cmd.roll
    pitch_cmd = attitude_cmd.pitch
    yaw_cmd = attitude_cmd.yaw
    
    # update controllers
    Fz = z_force_cmd
    Tx = roll_pid.update(pose.roll, attitude_cmd.roll, dt)
    Ty = pitch_pid.update(pose.pitch, attitude_cmd.pitch, dt)
    Tz = yaw_pid.update(pose.yaw, attitude_cmd.yaw, dt)
    
    # send command
    cmd = WrenchStamped()
    cmd.header.stamp = rospy.Time.now()
    cmd.wrench.force.z = Fz
    cmd.wrench.torque.x = Tx
    cmd.wrench.torque.y = Ty
    cmd.wrench.torque.z = Tz
    wrench_pub.publish(cmd)
    
    try:
      loop_rate.sleep()
    except rospy.ROSTimeMovedBackwardsException:
      pass
  

if __name__ == "__main__":
  main()
