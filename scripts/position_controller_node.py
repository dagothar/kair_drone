#!/usr/bin/python

import rospy
import numpy as np
from tf.transformations import *
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Float32
from kair_drone.msg import Pose
from pid.pid import PID
from dynamic_reconfigure.server import Server
from kair_drone.cfg import PositionControllersConfig
from kair_drone.srv import Reset, ResetResponse
from kair_drone.srv import Enable, EnableResponse
import math


pose = Pose()
position_cmd = Pose()
enabled = True


def pose_cb(msg):
  global pose
  pose = msg
  

def position_cmd_cb(msg):
  global position_cmd
  position_cmd = msg


def reset_handle(req, x_pid, y_pid):
  x_pid.reset()
  y_pid.reset()
  return ResetResponse()


def enable_handle(req, x_pid, y_pid):
  global enabled
  
  enabled = req.enable
  if enabled:
    x_pid.reset()
    y_pid.reset()
  return EnableResponse()


def reconfigure(config, level, x_pid, y_pid):
  x_pid._kp = config.x_P
  x_pid._ki = config.x_I
  x_pid._kd = config.x_D
  x_pid._lower = config.x_lo
  x_pid._upper = config.x_up
  
  y_pid._kp = config.y_P
  y_pid._ki = config.y_I
  y_pid._kd = config.y_D
  y_pid._lower = config.y_lo
  y_pid._upper = config.y_up
  
  return config
  

def main():
  rospy.init_node("position_controller")
  
  controllers = rospy.get_param("~position_controllers")  
  x_pid = PID(**controllers["x_pid"])
  y_pid = PID(**controllers["y_pid"])
  
  pose_sub = rospy.Subscriber("pose", Pose, pose_cb)
  position_cmd_sub = rospy.Subscriber("position_cmd", Pose, position_cmd_cb)
  attitude_cmd_pub = rospy.Publisher("attitude_cmd", Pose, queue_size=100)
  
  reset_srv = rospy.Service("~reset", Reset, lambda req: reset_handle(req, x_pid, y_pid))
  enable_srv = rospy.Service("~enable", Enable, lambda req: enable_handle(req, x_pid, y_pid))
  
  dynamic_reconfigure_server = Server(PositionControllersConfig, lambda c, l: reconfigure(c, l, x_pid, y_pid))
  
  loop_rate = rospy.Rate(100)
  while not rospy.is_shutdown():
    if enabled:
      
      yaw = pose.yaw
      
      # transform target to drone frame
      dx = position_cmd.x - pose.x
      dy = position_cmd.y - pose.y
      sy = math.sin(-yaw)
      cy = math.cos(-yaw)
      dx_drone = cy * dx - sy * dy
      dy_drone = sy * dx + cy * dy
      
      # update controllers
      pitch = -x_pid.update(0, dx_drone, 0.01)
      roll = -y_pid.update(0, dy_drone, 0.01)
      
      # send command
      cmd = Pose()
      cmd.z = position_cmd.z
      cmd.roll = roll
      cmd.pitch = pitch
      cmd.yaw = position_cmd.yaw
      attitude_cmd_pub.publish(cmd)
    
    try:
      loop_rate.sleep()
    except rospy.ROSTimeMovedBackwardsException:
      pass
  

if __name__ == "__main__":
  main()
