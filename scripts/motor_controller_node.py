#!/usr/bin/python

import rospy
import numpy as np
from geometry_msgs.msg import WrenchStamped
from kair_drone.msg import MotorSpeed
from kair_drone.srv import SetMaxMotorSpeed, SetMaxMotorSpeedResponse


wrench_msg = WrenchStamped()


def wrench_cb(msg):
  global wrench_msg
  wrench_msg = msg
  
  
def set_max_motor_speed_handle(req, motors):
  name_speed = zip(req.max_speed.name, req.max_speed.velocity)
  for name, speed in name_speed:
    for i in range(0, len(motors)):
      if motors[i]["name"] == name:
        motors[i]["max_w"] = speed if speed >= 0 else motors[i]["max_w"]

  return SetMaxMotorSpeedResponse()


def create_motor_matrix(m):
  # TODO: dynamic resize
  M = np.matrix([
    [m[0]["b"], m[1]["b"], m[2]["b"], m[3]["b"], m[4]["b"], m[5]["b"]],
    [m[0]["b"]*m[0]["y"], m[1]["b"]*m[1]["y"], m[2]["b"]*m[2]["y"], m[3]["b"]*m[3]["y"], m[4]["b"]*m[4]["y"], m[5]["b"]*m[5]["y"]],
    [m[0]["b"]*m[0]["x"], m[1]["b"]*m[1]["x"], m[2]["b"]*m[2]["x"], m[3]["b"]*m[3]["x"], m[4]["b"]*m[4]["x"], m[5]["b"]*m[5]["x"]],
    [m[0]["d"], m[1]["d"], m[2]["d"], m[3]["d"], m[4]["d"], m[5]["d"]]
  ])
  return M


def main():
  rospy.init_node("motor_controller")
  
  rate = rospy.get_param("~rate", 100)
  motors = rospy.get_param("~motors")
  M = create_motor_matrix(motors)
  Mplus = np.linalg.pinv(M)
  
  wrench_sub = rospy.Subscriber("wrench_cmd", WrenchStamped, wrench_cb)
  motor_speed_pub = rospy.Publisher("motor_speed_cmd", MotorSpeed, queue_size=100)
  max_motor_speed_srv = rospy.Service("set_max_motor_speed", SetMaxMotorSpeed, lambda req: set_max_motor_speed_handle(req, motors))
  
  loop_rate = rospy.Rate(rate)
  while not rospy.is_shutdown():
    
    # compute wrench
    W = np.array([wrench_msg.wrench.force.z, wrench_msg.wrench.torque.x, wrench_msg.wrench.torque.y, wrench_msg.wrench.torque.z])
    W.shape = (4, 1)
    w_squared = Mplus * W
    w = np.sqrt(np.clip(w_squared, 0.0, None))
    for i in range(0, 6):
      w[i] = w[i] if w[i] < motors[i]["max_w"] else motors[i]["max_w"]
      w[i] = np.sign(motors[i]["d"]) * w[i]
    
    # send wrench message
    msg = MotorSpeed()
    msg.name = [m["name"] for m in motors]
    msg.velocity = [float(v) for v in w]
    motor_speed_pub.publish(msg)
    
    try:
      loop_rate.sleep()
    except rospy.ROSTimeMovedBackwardsException:
      pass


if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    pass
