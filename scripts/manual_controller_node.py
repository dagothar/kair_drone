#!/usr/bin/python

import rospy
from std_msgs.msg import Float32
from sensor_msgs.msg import Joy
from kair_drone.msg import Pose
from geometry_msgs.msg import WrenchStamped
from kair_drone.srv import Reset, ResetResponse
from kair_drone.srv import Enable, EnableResponse
import math


joy_msg = Joy()
position_cmd = Pose()
enabled = True


def joy_cb(msg):
  global joy_msg
  
  joy_msg = msg
  

def reset_handle(req):
  return ResetResponse()


def enable_handle(req):
  global enabled
  
  enabled = req.enable
  
  return EnableResponse()
  

def main():
  rospy.init_node("manual_controller")
  
  rate = rospy.get_param("~rate", 100)
  params = rospy.get_param("~manual_controllers")  
  k_alt = params["k_alt"]
  c_alt = params["c_alt"]
  k_roll = params["k_roll"]
  k_pitch = params["k_pitch"]
  
  joy_sub = rospy.Subscriber("/joy", Joy, joy_cb)
  z_force_cmd_pub = rospy.Publisher("z_force_cmd", Float32, queue_size=100)
  att_cmd_pub = rospy.Publisher("attitude_cmd", Pose, queue_size=100)
  
  reset_srv = rospy.Service("~reset", Reset, lambda req: reset_handle(req))
  enable_srv = rospy.Service("~enable", Enable, lambda req: enable_handle(req))

  
  loop_rate = rospy.Rate(rate)
  while not rospy.is_shutdown():
    if enabled:

      try:
        att_cmd = Pose()
        att_cmd.roll = k_roll * joy_msg.axes[2]
        att_cmd.pitch = k_pitch * joy_msg.axes[3]
        att_cmd.yaw = 0.0
        att_cmd_pub.publish(att_cmd)
        
        z_force_cmd = Float32()
        z_force_cmd.data = k_alt * joy_msg.axes[1] + c_alt
        z_force_cmd_pub.publish(z_force_cmd)
      except IndexError:
        pass
    
    try:
      loop_rate.sleep()
    except rospy.ROSTimeMovedBackwardsException:
      pass
  

if __name__ == "__main__":
  main()
